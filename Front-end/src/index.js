/*!

=========================================================
* Argon Dashboard React - v1.1.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/argon-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter as Router, Route, Switch, Redirect } from "react-router-dom";
import { AuthContext} from "./views/context/auth";
import PrivateRoute from "./PrivateRoute";
import ListaVendas from "views/ListaVendas.js";
import Index from "views/Index";
import Login from "views/Login.js";

import "assets/plugins/nucleo/css/nucleo.css";
import "@fortawesome/fontawesome-free/css/all.min.css";
import "assets/scss/argon-dashboard-react.scss";

import AdminLayout from "layouts/Admin.js";
import AuthLayout from "layouts/Auth.js";

ReactDOM.render(
  <AuthContext.Provider value="false">
    <Router>
      <PrivateRoute component={Index} path="/admin" render={props => <AdminLayout {...props} />} />
      <PrivateRoute component={ListaVendas} path="/lista-vendas" render={props => <AdminLayout {...props} />} />
      <Route component={Login} path="/auth" render={props => <AuthLayout {...props} />} />
      {/* <Route path="/auth" render={props => <AuthLayout {...props} />} /> */}

      
      {/* <PrivateRoute from="/" to="/admin/index" /> */}
    
    </Router>
  </AuthContext.Provider>,
  document.getElementById("root")
);

//-----------------------------------------------------
// import React from "react";
// import ReactDOM from "react-dom";
// import { BrowserRouter as Router, Route, Switch, Redirect } from "react-router-dom";
// import { AuthContext} from "./views/context/auth";
// import PrivateRoute from "./PrivateRoute";
// import Index from "views/Index";
// import ListaVendas from "views/ListaVendas.js";

// import "assets/plugins/nucleo/css/nucleo.css";
// import "@fortawesome/fontawesome-free/css/all.min.css";
// import "assets/scss/argon-dashboard-react.scss";

// import AdminLayout from "layouts/Admin.js";
// import AuthLayout from "layouts/Auth.js";

// ReactDOM.render(
//   <AuthContext.Provider value="false">
//     {/* <Switch> */}
//     <Router>
//       <Route path="/auth" render={props => <AuthLayout {...props} />} />
//       {/* <PrivateRoute path="/admin" render={props => <AdminLayout {...props} />} /> */}
//       <PrivateRoute path="/admin/index" component={Index} render={props => <AdminLayout {...props} />} />
//       <PrivateRoute path="/admin/lista-vendas" component={ListaVendas} render={props => <AdminLayout {...props} />} />
//       {/* <PrivateRoute from="/" to="/admin/index" /> */}
//     </Router>
//     {/* </Switch> */}
//   </AuthContext.Provider>,
//   document.getElementById("root")
// );
