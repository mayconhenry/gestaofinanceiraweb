import React from "react";
// reactstrap components
import {
  Button,
  Card,
  CardBody,
  FormGroup,
  Input,
  InputGroupAddon,
  InputGroupText,
  InputGroup,
  Modal,
  Row,
  Col,
  Table
} from "reactstrap";

class CadastroVenda extends React.Component {
  state = {
    defaultModal: false
  };
  toggleModal = state => {
    this.setState({
      [state]: !this.state[state]
    });
  };
  render() {
    return (
      <>
        <Row>
          <Col xl="10">
            <Modal
              className="modal-dialog-centered   container"
              isOpen={this.props.isOpen}
              toggle={() => this.toggleModal("formModal")}
            >
              <div className="modal-body p-0" style={{width: '700px'}}>
                <Card className="bg-secondary shadow border-0">
                  <CardBody className="px-lg-5 py-lg-5">
                    <div className="text-muted text-center ">
                      Registro de venda
                    </div>
                    <hr className="my-4" />
                    <Row>
                        <Col md="12">
                            <FormGroup className="mb-3">
                                <InputGroup className="input-group-alternative">
                                    <InputGroupAddon addonType="prepend">
                                        <InputGroupText>
                                            <i className="ni ni-single-02" />
                                        </InputGroupText>
                                    </InputGroupAddon>
                                    <Input placeholder="Cliente" type="text" />
                                </InputGroup>
                            </FormGroup>
                        </Col>
                    </Row>
                    <Row>
                        <Col md="6">
                            <FormGroup className="mb-3">
                                <InputGroup className="input-group-alternative">
                                    <InputGroupAddon addonType="prepend">
                                        <InputGroupText>
                                            <i className="ni ni-money-coins" />
                                        </InputGroupText>
                                    </InputGroupAddon>
                                    <Input type="select">
                                      <option>Tipo de venda</option>
                                      <option>Crédito</option>
                                      <option>Débito</option>
                                      <option>Dinheiro</option>
                                    </Input>
                                </InputGroup>
                            </FormGroup>
                        </Col>
                        <Col md="6">
                            <FormGroup className="mb-3">
                                <InputGroup className="input-group-alternative">
                                    <InputGroupAddon addonType="prepend">
                                        <InputGroupText>
                                            <i className="fa fa-credit-card" />
                                        </InputGroupText>
                                    </InputGroupAddon>
                                    <Input type="select">
                                          <option>Nº Parcelas</option>
                                          <option>1x</option>
                                          <option>2x</option>
                                          <option>3x</option>
                                    </Input>
                                </InputGroup>
                            </FormGroup>
                        </Col>
                    </Row>
                    <Row>
                        <Col md="6">
                            <FormGroup className="mb-3">
                                <InputGroup className="input-group-alternative">
                                    <InputGroupAddon addonType="prepend">
                                        <InputGroupText>
                                            <i className="fa fa-money-bill-alt" />
                                        </InputGroupText>
                                    </InputGroupAddon>
                                    <Input placeholder="R$ de desconto" type="text" />
                                </InputGroup>
                            </FormGroup>
                        </Col>
                        <Col md="6">
                            <FormGroup className="mb-3">
                                <InputGroup className="input-group-alternative">
                                    <InputGroupAddon addonType="prepend">
                                        <InputGroupText>
                                            <i className="fa fa-donate" />
                                        </InputGroupText>
                                    </InputGroupAddon>
                                    <Input placeholder="% de desconto" type="text" />
                                </InputGroup>
                            </FormGroup>
                        </Col>
                    </Row>
                    <hr className="my-4" />
                    <div className="text-muted text-center my-4">
                        Adicionar produtos
                    </div>
                    <Row>
                        <Col md="12">
                            <FormGroup className="mb-3">
                                <InputGroup className="input-group-alternative">
                                    <InputGroupAddon addonType="prepend">
                                        <InputGroupText>
                                            <i className="fa fa-address-book" />
                                        </InputGroupText>
                                    </InputGroupAddon>
                                    <Input type="select">
                                        <option>Colaborador</option>
                                        <option>Elaine</option>
                                        <option>Robson</option>
                                        <option>Lurdinha</option>
                                    </Input>
                              </InputGroup>
                          </FormGroup>
                        </Col>
                    </Row>
                    <Row>
                        <Col md="4">
                            <FormGroup className="mb-3">
                                <InputGroup className="input-group-alternative">
                                    <InputGroupAddon addonType="prepend">
                                        <InputGroupText>
                                            <i className="fa fa-cart-arrow-down" />
                                        </InputGroupText>
                                    </InputGroupAddon>
                                    <Input type="select">
                                        <option>Produto</option>
                                        <option>Escovinha</option>
                                        <option>Unha</option>
                                        <option>Corte</option>
                                    </Input>
                              </InputGroup>
                          </FormGroup>
                        </Col>
                        <Col md="4">
                            <FormGroup className="mb-3">
                                <InputGroup className="input-group-alternative">
                                    <InputGroupAddon addonType="prepend">
                                        <InputGroupText>
                                            <i className="fa fa-chart-pie" />
                                        </InputGroupText>
                                    </InputGroupAddon>
                                    <Input placeholder="Comissão" type="text" />
                                </InputGroup>
                            </FormGroup>
                        </Col>
                        <Col md="4">
                            <FormGroup className="mb-3">
                                <InputGroup className="input-group-alternative">
                                    <InputGroupAddon addonType="prepend">
                                        <InputGroupText>
                                            <i className="fa fa-subscript" />
                                        </InputGroupText>
                                    </InputGroupAddon>
                                    <Input type="select">
                                          <option>Qtde</option>
                                          <option>1</option>
                                          <option>2</option>
                                          <option>3</option>
                                    </Input>
                                </InputGroup>
                            </FormGroup>
                        </Col>
                    </Row>
                    <Row>
                        <Col md="4">
                            <FormGroup className="mb-3">
                                <InputGroup className="input-group-alternative">
                                    <InputGroupAddon addonType="prepend">
                                        <InputGroupText>
                                            <i className="fa fa-money-bill-alt" />
                                        </InputGroupText>
                                    </InputGroupAddon>
                                    <Input placeholder="Valor unitário" type="text" />
                                </InputGroup>
                            </FormGroup>
                        </Col>
                        <Col md="4">
                            <FormGroup className="mb-3">
                                <InputGroup className="input-group-alternative">
                                    <InputGroupAddon addonType="prepend">
                                        <InputGroupText>
                                            <i className="fa fa-minus" />
                                        </InputGroupText>
                                    </InputGroupAddon>
                                    <Input placeholder="Custos adicionais" type="text" />
                                </InputGroup>
                            </FormGroup>
                        </Col>
                        <Col md="4">
                            <Button
                              color="warning"
                              type="button"
                              block
                              onClick={this.props.fechar}
                            >
                              Incluir produto
                          </Button>
                        </Col>
                    </Row>
                    <Table className="align-items-center table-flush" responsive>
                        <thead className="thead-light">
                          <tr>
                            <th scope="col" className="text-center">Colaborador</th>
                            <th scope="col" className="text-center">Comissão</th>
                            <th scope="col" className="text-center">Qtde</th>
                            <th scope="col" className="text-center">Produto</th>
                            <th scope="col" className="text-right">Valor</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                              <td className="text-center">Lurdinha</td>
                              <td className="text-center">30%</td>
                              <td className="text-center">1</td>
                              <td className="text-center">Escovinha</td>
                              <td className="text-right">R$ 100,00</td>
                          </tr>
                          <tr>
                              <td className="text-center">Lurdinha</td>
                              <td className="text-center">30%</td>
                              <td className="text-center">1</td>
                              <td className="text-center">Corte</td>
                              <td className="text-right">R$ 25,00</td>
                          </tr>
                          <tr>
                              <td className="text-center">Elaine</td>
                              <td className="text-center">50%</td>
                              <td className="text-center">1</td>
                              <td className="text-center">Unha</td>
                              <td className="text-right">R$ 20,00</td>
                          </tr>
                        </tbody>
                    </Table>
                    <Row>
                      <Col>
                        <h3 className="text-right">Total R$ 145,00</h3>
                      </Col>
                    </Row>

                    <div className="text-right">
                      <Button
                        className="my-4"
                        color="danger"
                        type="button"
                        onClick={this.props.fechar}
                      >
                        Cancelar
                      </Button>
                      <Button
                        className="my-4"
                        color="success"
                        type="button"
                        onClick={this.props.fechar}
                      >
                        Salvar
                      </Button>
                    </div>
                  </CardBody>
                </Card>
              </div>
            </Modal>
          </Col>
        </Row>
      </>
    );
  }
}

export default CadastroVenda;