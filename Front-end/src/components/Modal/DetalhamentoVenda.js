import React from "react";
// reactstrap components
import {
  Button,
  Card,
  CardBody,
  Row,
  Col,
  Table,
  Modal
} from "reactstrap";

class DetalhamentoVenda extends React.Component {
  state = {
    defaultModal: false
  };
  toggleModal = state => {
    this.setState({
      [state]: !this.state[state]
    });
  };
  render() {
    return (
      <>
        <Row>
          <Col xl="10">
            <Modal
              className="modal-dialog-centered   container"
              isOpen={this.props.isOpen}
              toggle={() => this.toggleModal("formModal")}
            >
              <div className="modal-body p-0" style={{width: '700px'}}>
                <Card className="bg-secondary shadow border-0">
                  <CardBody className="px-lg-5 py-lg-5">
                    <div className="text-muted text-center ">
                      Resumo da venda
                    </div>
                    <hr className="my-4" />
                    {/* md="12" */}
                    <Row>
                        <Col>
                            <Button color="success my-2" block outline type="button">
                                Dedução de lucro R$ 40,00
                            </Button>
                        </Col>
                        <Col>
                            <Button color="danger my-2" block outline type="button">
                                Despesas totais R$ 93,80
                            </Button>
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            <div className="text-left">
                                <h3>Cliente: <span className="font-weight-light">Denise da Rocha</span></h3>
                                <h3>Pagamento: <span className="font-weight-light">Visa crédito 3x</span></h3>
                                <h3>Desconto: <span className="font-weight-light">R$5,00</span></h3>
                                <h3>Custos adicionais: <span className="font-weight-light">R$35,00</span></h3>
                            </div>
                        </Col>
                        <Col>
                            <div className="text-left">
                                <h3>Comissão de colaborador: <span className="font-weight-light">R$40,00</span></h3>
                                <h3>Taxa stone: <span className="font-weight-light">R$2,00 (1.5%)</span></h3>
                                <h3>Taxa de antecipação: <span className="font-weight-light">R$6,30( 4.8%)</span></h3>
                                <h3>Simples nacional: <span className="font-weight-light">R$4,30 (6.8%)</span></h3>
                            </div>
                        </Col>
                    </Row>

                    <div className="text-muted text-left my-2">
                        Comissão
                    </div>
                    <Table className="align-items-center table-flush" responsive>
                        <thead className="thead-light">
                          <tr>
                            <th scope="col" className="text-center">Colaborador</th>
                            <th scope="col" className="text-center">Valor</th>
                            <th scope="col" className="text-center">Comissão</th>
                            <th scope="col" className="text-right">Valor da comissão</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                              <td className="text-center">Lurdinha</td>
                              <td className="text-center">R$ 100,00</td>
                              <td className="text-center">30%</td>
                              <td className="text-right">R$ 30,00</td>
                          </tr>
                          <tr>
                              <td className="text-center">Elaine</td>
                              <td className="text-center">R$ 20,00</td>
                              <td className="text-center">50%</td>
                              <td className="text-right">R$ 10,00</td>
                          </tr>
                        </tbody>
                    </Table>
                    <Row>
                      <Col>
                        <h3 className="text-right">Total R$ 40,00</h3>
                      </Col>
                    </Row>

                    {/* PRODUTOS E SERVIÇOS */}
                    <div className="text-muted text-left my-2">
                      Produtos/Serviços
                    </div>
                    <Table className="align-items-center table-flush" responsive>
                        <thead className="thead-light">
                          <tr>
                            <th scope="col" className="text-center">Colaborador</th>
                            <th scope="col" className="text-center">Comissão</th>
                            <th scope="col" className="text-center">Qtde</th>
                            <th scope="col" className="text-center">Produto</th>
                            <th scope="col" className="text-right">Valor</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                              <td className="text-center">Lurdinha</td>
                              <td className="text-center">30%</td>
                              <td className="text-center">1</td>
                              <td className="text-center">Escovinha</td>
                              <td className="text-right">R$ 100,00</td>
                          </tr>
                          <tr>
                              <td className="text-center">Lurdinha</td>
                              <td className="text-center">30%</td>
                              <td className="text-center">1</td>
                              <td className="text-center">Corte</td>
                              <td className="text-right">R$ 25,00</td>
                          </tr>
                          <tr>
                              <td className="text-center">Elaine</td>
                              <td className="text-center">50%</td>
                              <td className="text-center">1</td>
                              <td className="text-center">Unha</td>
                              <td className="text-right">R$ 20,00</td>
                          </tr>
                        </tbody>
                    </Table>
                    <Row>
                      <Col>
                        <h3 className="text-right">Total R$ 145,00</h3>
                      </Col>
                    </Row>

                    <div className="text-right">
                        <Button
                                className="my-4"
                                color="danger"
                                type="button"
                                onClick={this.props.fechar}> 
                            Sair
                        </Button>
                    </div>
                  </CardBody>
                </Card>
              </div>
            </Modal>
          </Col>
        </Row>
      </>
    );
  }
}

export default DetalhamentoVenda;