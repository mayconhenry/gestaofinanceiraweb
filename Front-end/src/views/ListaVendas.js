/*!

=========================================================
* Argon Dashboard React - v1.1.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/argon-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React from "react";
// javascipt plugin for creating charts
import Chart from "chart.js";
// reactstrap components
import {
  Button,
  Card,
  CardHeader,
  Table,
  Container,
  Row,
  Col
} from "reactstrap";

// core components
import {
  chartOptions,
  parseOptions,
} from "variables/charts.js";
import CadastroVenda from "components/Modal/CadastroVenda";
import DetalhamentoVenda from "components/Modal/DetalhamentoVenda";

class listaVendas extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      activeNav: 1,
      chartExample1Data: "data1",
      isModalEditarOpen: false,
      isModalDetalhamentoOpen: false
    };
    if (window.Chart) {
      parseOptions(Chart, chartOptions());
    }
  }

  componentDidMount() {
      // axios.post("http://localhost:3000/auth/login", {
      //   username, 
      //   password
      // }).then(result => {
      //   if (result.status === 201) {
      //     setLoggedIn(true);
      //     setAuthTokens(result.data.access_token);
      //   } else {
      //     setLoggedIn(false);
      //     setIsError(true);
      //   }
      // }).catch(e => {
      //   setIsError(true);
      // });
      // // setLoggedIn(true);
      // console.log(isLoggedIn);
    
  // };
  }
  abrirModalDetalhamento = () => {
    this.setState({isModalDetalhamentoOpen: !this.state.isModalDetalhamentoOpen});
  }
  
  cadastroVenda = () => {
    this.setState({isModalEditarOpen: !this.state.isModalEditarOpen});
  }
  
  fecharEditarVenda = () => {
    this.setState({isModalEditarOpen: !this.state.isModalEditarOpen});
  }

  fecharDetalhamentoVenda = () => {
    this.setState({isModalDetalhamentoOpen: !this.state.isModalDetalhamentoOpen});
  }

  render() {
    console.log(`${this.url}`);
    return (
      <>
        <div className="header bg-gradient-info pb-8 pt-5 pt-md-8">          
        </div>

        {/* Page content */}
        <Container className="mt--7" fluid>
          <Row className="mt-2">
            <Col className="mb-5 mb-xl-0" xl="12">
              <Card className="shadow">
                <CardHeader className="border-0">
                  <Row className="align-items-center">
                    <div className="col">
                      <h3 className="mb-0">Outrubro de 2020</h3>
                    </div>
                    <div className="col text-right">
                      <Button
                        color="success"
                        onClick={this.cadastroVenda}
                      >
                        Registrar venda
                      </Button>
                    </div>
                  </Row>
                </CardHeader>
                <Table className="align-items-center table-flush" responsive>
                  <thead className="thead-light">
                    <tr>
                      <th scope="col">#</th>
                      <th scope="col" className="text-center">Data</th>
                      <th scope="col" className="text-center">Cliente</th>
                      <th scope="col" className="text-center">Tipo de venda</th>
                      <th scope="col" className="text-right">Valor</th>
                      <th scope="col"className="text-center">Ações</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                        <th scope="row">1</th>
                        <td className="text-center">26/10/2020</td>
                        <td className="text-center">Kátia</td>
                        <td className="text-center">Crédito</td>
                        <td className="text-right">R$ 100,00</td>
                        <td className="text-center">
                            <i onClick={this.abrirModalDetalhamento} className="fa fa-info-circle fa-2x text-info"></i>
                            <i onClick={this.cadastroVenda}  className="fas fa-edit fa-2x text-gray-dark mx-2"></i>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">2</th>
                        <td className="text-center">25/10/2020</td>
                        <td className="text-center">Francisca</td>
                        <td className="text-center">Débito</td>
                        <td className="text-right">R$ 180,00</td>
                        <td className="text-center">
                            <i className="fa fa-info-circle fa-2x text-info"></i>
                            <i className="fas fa-edit fa-2x text-gray-dark mx-2"></i>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">3</th>
                        <td className="text-center">24/10/2020</td>
                        <td className="text-center">Henrique</td>
                        <td className="text-center">Dinheiro</td>
                        <td className="text-right">R$ 110,00</td>
                        <td className="text-center">
                            <i className="fa fa-info-circle fa-2x text-info"></i>
                            <i className="fas fa-edit fa-2x text-gray-dark mx-2"></i>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">4</th>
                        <td className="text-center">23/10/2020</td>
                        <td className="text-center">Paulo Cesar</td>
                        <td className="text-center">Crédito 2x</td>
                        <td className="text-right">R$ 70,00</td>
                        <td className="text-center">
                            <i className="fa fa-info-circle fa-2x text-info"></i>
                            <i className="fas fa-edit fa-2x text-gray-dark mx-2"></i>
                        </td>
                    </tr>
                  </tbody>
                </Table>
              </Card>
            </Col>
          </Row>
        </Container>
        <CadastroVenda isOpen={this.state.isModalEditarOpen} fechar={this.fecharEditarVenda}/>
        <DetalhamentoVenda isOpen={this.state.isModalDetalhamentoOpen} fechar={this.fecharDetalhamentoVenda}/>
      </>
    );
  }
}

export default listaVendas;
