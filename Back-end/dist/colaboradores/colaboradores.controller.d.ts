import { ColaboradoresService } from './colaboradores.service';
import { ColaboradoresEntity } from './entities/colaboradores.entity';
export declare class ColaboradoresController {
    private colaboradoresService;
    constructor(colaboradoresService: ColaboradoresService);
    index(): Promise<ColaboradoresEntity[]>;
}
