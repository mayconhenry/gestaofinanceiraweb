"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ColaboradoresController = void 0;
const common_1 = require("@nestjs/common");
const colaboradores_service_1 = require("./colaboradores.service");
let ColaboradoresController = class ColaboradoresController {
    constructor(colaboradoresService) {
        this.colaboradoresService = colaboradoresService;
    }
    index() {
        return this.colaboradoresService.findAll();
    }
};
__decorate([
    common_1.Get(),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], ColaboradoresController.prototype, "index", null);
ColaboradoresController = __decorate([
    common_1.Controller('colaboradores'),
    __metadata("design:paramtypes", [colaboradores_service_1.ColaboradoresService])
], ColaboradoresController);
exports.ColaboradoresController = ColaboradoresController;
//# sourceMappingURL=colaboradores.controller.js.map