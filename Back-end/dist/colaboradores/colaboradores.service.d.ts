import { ColaboradoresEntity } from './entities/colaboradores.entity';
export declare class ColaboradoresService {
    findAll(): Promise<ColaboradoresEntity[]>;
}
