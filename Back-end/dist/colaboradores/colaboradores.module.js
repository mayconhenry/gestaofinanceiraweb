"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ColaboradoresModule = void 0;
const common_1 = require("@nestjs/common");
const colaboradores_service_1 = require("./colaboradores.service");
const colaboradores_controller_1 = require("./colaboradores.controller");
const typeorm_1 = require("@nestjs/typeorm");
const colaboradores_entity_1 = require("./entities/colaboradores.entity");
const colaboradores_repository_1 = require("./entities/colaboradores.repository");
let ColaboradoresModule = class ColaboradoresModule {
};
ColaboradoresModule = __decorate([
    common_1.Module({
        imports: [typeorm_1.TypeOrmModule.forFeature([colaboradores_entity_1.ColaboradoresEntity, colaboradores_repository_1.ColaboradoresRepository])],
        controllers: [colaboradores_controller_1.ColaboradoresController],
        providers: [colaboradores_service_1.ColaboradoresService]
    })
], ColaboradoresModule);
exports.ColaboradoresModule = ColaboradoresModule;
//# sourceMappingURL=colaboradores.module.js.map