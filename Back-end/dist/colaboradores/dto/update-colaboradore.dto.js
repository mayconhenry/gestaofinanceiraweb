"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UpdateColaboradoreDto = void 0;
const mapped_types_1 = require("@nestjs/mapped-types");
const create_colaboradore_dto_1 = require("./create-colaboradore.dto");
class UpdateColaboradoreDto extends mapped_types_1.PartialType(create_colaboradore_dto_1.CreateColaboradoreDto) {
}
exports.UpdateColaboradoreDto = UpdateColaboradoreDto;
//# sourceMappingURL=update-colaboradore.dto.js.map