"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AppModule = void 0;
const common_1 = require("@nestjs/common");
const core_1 = require("@nestjs/core");
const typeorm_1 = require("@nestjs/typeorm");
const orm_config_1 = require("./orm.config");
const app_controller_1 = require("./app.controller");
const app_service_1 = require("./app.service");
const colaboradores_module_1 = require("./colaboradores/colaboradores.module");
const auth_module_1 = require("./auth/auth.module");
const users_module_1 = require("./users/users.module");
const jwt_auth_guard_1 = require("./auth/jwt-auth.guard");
const vendas_module_1 = require("./vendas/vendas.module");
const itens_module_1 = require("./itens/itens.module");
const tipos_venda_module_1 = require("./tipos-venda/tipos-venda.module");
let AppModule = class AppModule {
};
AppModule = __decorate([
    common_1.Module({
        imports: [
            typeorm_1.TypeOrmModule.forRoot(orm_config_1.config),
            colaboradores_module_1.ColaboradoresModule, auth_module_1.AuthModule, users_module_1.UsersModule, vendas_module_1.VendasModule, itens_module_1.ItensModule, tipos_venda_module_1.TiposVendaModule
        ],
        controllers: [app_controller_1.AppController],
        providers: [
            app_service_1.AppService,
            {
                provide: core_1.APP_GUARD,
                useClass: jwt_auth_guard_1.JwtAuthGuard
            }
        ],
    })
], AppModule);
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map