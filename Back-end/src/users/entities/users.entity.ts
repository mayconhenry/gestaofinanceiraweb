import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity('users')
export class UsersEntity {
    
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ unique: true,  nullable: false})
    login: string;

    @Column({nullable: false})
    password: string;

    @Column({ unique: true, nullable: false })
    email: string;
}
