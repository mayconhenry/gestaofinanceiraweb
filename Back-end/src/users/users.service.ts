import { Injectable } from '@nestjs/common';
import { getCustomRepository } from 'typeorm';
import { UsersEntity } from './entities/users.entity';
import { UsersRepository } from './entities/users.repository';

// This should be a real class/interface representing a user entity
export type User = any;

@Injectable()
export class UsersService {
  private readonly users: User[];

  constructor() {
    
  }

  async findOne(username: string): Promise<User | undefined> {
    const usersRepository = getCustomRepository(UsersRepository);
    return await usersRepository.findOneOrFail({login:username});
  }
}