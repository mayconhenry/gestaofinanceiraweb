import { Module } from '@nestjs/common';
import { APP_GUARD } from '@nestjs/core';
import { TypeOrmModule } from '@nestjs/typeorm';
import { config } from './orm.config';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ColaboradoresModule } from './colaboradores/colaboradores.module';
import { AuthModule } from './auth/auth.module';
import { UsersModule } from './users/users.module';
import { JwtAuthGuard } from './auth/jwt-auth.guard';
import { VendasModule } from './vendas/vendas.module';
import { ItensModule } from './itens/itens.module';
import { TiposVendaModule } from './tipos-venda/tipos-venda.module';

@Module({
  imports: [
    TypeOrmModule.forRoot(config), 
    ColaboradoresModule, AuthModule, UsersModule, VendasModule, ItensModule, TiposVendaModule
  ],
  controllers: [AppController],
  providers: [
    AppService,
    {
      provide:APP_GUARD,
      useClass: JwtAuthGuard
    }
  ],
})
export class AppModule {}
