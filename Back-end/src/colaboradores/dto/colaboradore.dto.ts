import { ApiProperty } from '@nestjs/swagger';

export class ColaboradoreDto {
    
    @ApiProperty()
    readonly id: number;
    
    @ApiProperty()
    readonly id_organizacao: number;

    @ApiProperty()
    readonly nome: string;
    
}