import { Controller, Get} from '@nestjs/common';
import { ColaboradoresService } from './colaboradores.service';
import { ColaboradoresEntity } from './entities/colaboradores.entity';

@Controller('colaboradores')
export class ColaboradoresController {
    constructor(private colaboradoresService: ColaboradoresService) {}

    @Get()
      index(): Promise<ColaboradoresEntity[]> {
      return this.colaboradoresService.findAll();
    }

}

