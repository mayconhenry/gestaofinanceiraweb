import { Injectable } from '@nestjs/common';
import { getCustomRepository } from 'typeorm';
import { ColaboradoresEntity } from './entities/colaboradores.entity';
import { ColaboradoresRepository } from './entities/colaboradores.repository';

@Injectable()
export class ColaboradoresService {

  // constructor(
  //     @InjectRepository(ColaboradoresEntity)
  //     private sera: AbstractRepository<ColaboradoresRepository>
  // ) { }

  async findAll(): Promise<ColaboradoresEntity[]>{
    const colaboradoresRepository = getCustomRepository(ColaboradoresRepository);
    return await colaboradoresRepository.find();
  }
}