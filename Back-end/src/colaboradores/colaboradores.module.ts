import { Module } from '@nestjs/common';
import { ColaboradoresService } from './colaboradores.service';
import { ColaboradoresController } from './colaboradores.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ColaboradoresEntity } from './entities/colaboradores.entity';
import { ColaboradoresRepository } from './entities/colaboradores.repository';

@Module({
  imports:[TypeOrmModule.forFeature([ColaboradoresEntity, ColaboradoresRepository])],
  controllers: [ColaboradoresController],
  providers: [ColaboradoresService]
})
export class ColaboradoresModule {}
