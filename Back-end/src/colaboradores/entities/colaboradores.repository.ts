import {AbstractRepository, EntityRepository, Repository} from "typeorm";
import {ColaboradoresEntity} from "./colaboradores.entity";

@EntityRepository(ColaboradoresEntity)
export class ColaboradoresRepository extends Repository<ColaboradoresEntity> {
   
}