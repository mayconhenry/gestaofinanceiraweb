import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity('colaboradores')
export class ColaboradoresEntity {
    
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    id_organizacao: number;

    @Column()
    nome: string;

}
