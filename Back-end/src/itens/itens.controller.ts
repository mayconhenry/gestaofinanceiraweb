import { Controller, Get } from '@nestjs/common';
import { ItensEntity } from './entities/itens.entity';
import { ItensService } from './itens.service';

@Controller('itens')
export class ItensController {

    constructor(private itensService: ItensService){}

    @Get()
    index(): Promise<ItensEntity[]>{
        return  this.itensService.findAll();
    }

}
