import {AbstractRepository, EntityRepository, Repository} from "typeorm";
import {ItensEntity} from "./itens.entity";

@EntityRepository(ItensEntity)
export class ItensRepository extends Repository<ItensEntity> {
}