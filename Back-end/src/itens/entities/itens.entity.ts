import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity('itens')
export class ItensEntity {
    
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    id_organizacao: number;

    @Column()
    nome: string;

    @Column({nullable: true})
    comissao: number;
}
