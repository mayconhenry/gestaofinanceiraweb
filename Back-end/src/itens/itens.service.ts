import { Injectable } from '@nestjs/common';
import { getCustomRepository } from 'typeorm';
import { ItensEntity } from './entities/itens.entity';
import { ItensRepository } from './entities/itens.repository';

@Injectable()
export class ItensService {
    async findAll(): Promise<ItensEntity[]>{
        const itensRepository = getCustomRepository(ItensRepository);
        return await itensRepository.find();
    }
}
