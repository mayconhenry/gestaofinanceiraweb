import { Module } from '@nestjs/common';
import { ItensService } from './itens.service';
import { ItensController } from './itens.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ItensEntity } from './entities/itens.entity';
import { ItensRepository } from './entities/itens.repository';

@Module({
  imports:[TypeOrmModule.forFeature([ItensEntity, ItensRepository])],
  controllers: [ItensController],
  providers:[ItensService]
})
export class ItensModule {}
