import { Controller, Get } from '@nestjs/common';
import { TiposVendaEntity } from './entities/tipos-venda.entity';
import { TiposVendaService } from './tipos-venda.service';

@Controller('tipos-venda')
export class TiposVendaController {
    constructor(private tiposVendaService: TiposVendaService){}

    @Get()
    index(): Promise<TiposVendaEntity[]>{
       return this.tiposVendaService.findAll(); 
    }
}
