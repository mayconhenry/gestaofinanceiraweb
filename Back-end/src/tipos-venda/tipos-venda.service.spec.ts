import { Test, TestingModule } from '@nestjs/testing';
import { TiposVendaService } from './tipos-venda.service';

describe('TiposVendaService', () => {
  let service: TiposVendaService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [TiposVendaService],
    }).compile();

    service = module.get<TiposVendaService>(TiposVendaService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
