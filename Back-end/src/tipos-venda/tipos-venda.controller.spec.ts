import { Test, TestingModule } from '@nestjs/testing';
import { TiposVendaController } from './tipos-venda.controller';

describe('TiposVendaController', () => {
  let controller: TiposVendaController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [TiposVendaController],
    }).compile();

    controller = module.get<TiposVendaController>(TiposVendaController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
