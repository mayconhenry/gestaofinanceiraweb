import { Column, Double, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity('tipos_venda')
         
export class TiposVendaEntity {
    
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    id_organizacao: number;

    @Column()
    nome: string;

    @Column()
    taxa: number;
}
