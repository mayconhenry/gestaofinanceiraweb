import {AbstractRepository, EntityRepository, Repository} from "typeorm";
import { TiposVendaEntity } from "./tipos-venda.entity";

@EntityRepository(TiposVendaEntity)
export class TiposVendaRepository extends Repository<TiposVendaEntity> {
}