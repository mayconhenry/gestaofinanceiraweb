import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TiposVendaEntity } from './entities/tipos-venda.entity';
import { TiposVendaRepository } from './entities/tipos-venda.repository';
import { TiposVendaController } from './tipos-venda.controller';
import { TiposVendaService } from './tipos-venda.service';

@Module({
  imports:[TypeOrmModule.forFeature([TiposVendaEntity, TiposVendaRepository])],
  controllers: [TiposVendaController],
  providers:[TiposVendaService]
})
export class TiposVendaModule {}
