import { Injectable } from '@nestjs/common';
import { getCustomRepository } from 'typeorm';
import { TiposVendaEntity } from './entities/tipos-venda.entity';
import { TiposVendaRepository } from './entities/tipos-venda.repository';

@Injectable()
export class TiposVendaService {
    async findAll(): Promise<TiposVendaEntity[]>{
        const tiposVendaRepository = getCustomRepository(TiposVendaRepository);
        return await tiposVendaRepository.find();
    }
}
