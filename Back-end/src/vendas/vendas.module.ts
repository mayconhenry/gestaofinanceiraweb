import { Module } from '@nestjs/common';
import { VendasController } from './vendas.controller';
import { VendasService } from './vendas.service';
import { VendasEntity } from './entities/vendas.entity';
import { VendasRepository } from './entities/vendas.repository';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports:[TypeOrmModule.forFeature([VendasEntity, VendasRepository])],
  controllers: [VendasController],
  providers:[VendasService]
})
export class VendasModule {}