import { Controller, Get } from '@nestjs/common';
import { VendasService } from './vendas.service';
import { VendasEntity } from './entities/vendas.entity';

@Controller('vendas')
export class VendasController {
    constructor(private vendasService: VendasService) {}

    @Get()
      index(): Promise<VendasEntity[]> {
      return this.vendasService.findAll();
    }
}
