import { Injectable } from '@nestjs/common';
import { VendasEntity } from 'src/vendas/entities/vendas.entity';
import { VendasRepository } from 'src/vendas/entities/vendas.repository';
import { getCustomRepository } from 'typeorm';

@Injectable()
export class VendasService {
    async findAll(): Promise<VendasEntity[]>{
        const colaboradoresRepository = getCustomRepository(VendasRepository);
        return await colaboradoresRepository.find();
    }
}
