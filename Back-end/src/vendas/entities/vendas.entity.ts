import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity('vendas')
export class VendasEntity {
    
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    cliente: string;

    @Column()
    id_tipo_venda: number;

    @Column()
    numero_parcelas: number;

    @Column({ nullable: true })
    desconto_valor: number;

    @Column({ nullable: true })
    desconto_porcentagem: number;

    @Column({ type: 'date', nullable: true })
    data_compra: Date;
}
