import {AbstractRepository, EntityRepository, Repository} from "typeorm";
import {VendasEntity} from "./vendas.entity";

@EntityRepository(VendasEntity)
export class VendasRepository extends Repository<VendasEntity> {
   
}